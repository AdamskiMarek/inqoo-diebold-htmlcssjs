products = [
  {
    id: 1,
    productsName: "piłka",
    price_net: 100,
  },

  {
    id: 2,
    productsName: "kosz",
    price_net: 200,
  },

  {
    id: 5,
    productsName: "krzesło",
    price_net: 500,
  },
];

var tbody = document.getElementById("productsList");

function renderProducts(products) {
  tbody.innerHTML = ``;
  for (let product of products) {
    createProduct(product);
  }
}

function createProduct(product) {
  var item = `
  <tr data-product-id=${product.id}>
 <td> ${product.id} </td>
 <td> ${product.productsName} </td>
 <td> ${product.price_net}</td>
 </tr>
`;
  tbody.innerHTML += item;
}
renderProducts(products);

var filterProducts = document.querySelector(".filter");

// filterProducts.onclick = function(products) {
//   var minPrice = document.getElementsByClassName('minPrice');
//   var maxPrice = document.getElementsByClassName('maxPrice');
//   filterProductsByPrice(products, minPrice, maxPrice);
// };

filterProducts.addEventListener("click", () => {
  var minPrice = document.querySelector(".minPrice").value;
  var maxPrice = document.querySelector(".maxPrice").value;
  filterProductsByPrice(products, minPrice, maxPrice);
});

function filterProductsByPrice(products, minPrice, maxPrice) {
  var productsByPrice = [];
  for (let product of products) {
    if (product.price_net > minPrice && product.price_net < maxPrice) {
      productsByPrice.push(product);
    }
  }
  renderProducts(productsByPrice);
}

tbody.addEventListener("click", function (event) {
  document.querySelectorAll("#productsList tr").forEach((element) => {
    element.classList.remove("active");
  });
  event.target.closest("[data-product-id]").classList.add("active");
});

var form = document.querySelector("#product-editor form")
console.log(form);