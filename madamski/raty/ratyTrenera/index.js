
var loan_amount = document.getElementById('loan_amount')
var loan_months = document.getElementById('loan_months')
var loan_interest = document.getElementById('loan_interest')

document.getElementById('calculateBtn').addEventListener('click', function (event) {
  event.preventDefault()

  calculateFromUserInput()
})

/* Example loan */
calculateExample()


function calculateExample() {
  var example_loan = {
    amount: 100000,
    months: 12,
    interest: 10
  }
  var payments = calculate(example_loan)
  renderPayments(payments)
  console.table(payments)
}

function calculateFromUserInput() {
  var user_loan = {
    amount: loan_amount.valueAsNumber * 100,
    months: loan_months.valueAsNumber,
    interest: loan_interest.valueAsNumber
  }
  var payments = calculate(user_loan)
  renderPayments(payments)
}

function calculate(loan) {
  var payments = []
  var capital_payment = Math.ceil(loan.amount / loan.months)
  var amount_left = loan.amount;

  for (var i = 0; i < loan.months; i++) {
    var interest_payment = Math.ceil((amount_left / 12) * (loan.interest / 100))

    amount_left -= capital_payment

    payments.push({
      i: i + 1,
      capital_payment,
      interest_payment,
      amount_left,
      payment: capital_payment + interest_payment
    })
  }
  return payments;
}

function formatCurrency(cents) {
  return (cents / 100).toFixed(2) + ' PLN'
}

function renderTotals(payments) {
  var totals = {
    payments: 0, capital: 0, interest: 0
  }
  for (let payment of payments) {
    totals.payments += payment.payment
    totals.capital += payment.capital_payment
    totals.interest += payment.interest_payment
  }
  return totals
}

function renderPayments(payments) {
  var tbody = document.querySelector('.js-payments-table tbody')
  tbody.innerHTML = ''

  for (let payment of payments) {
    // var tr = document.createElement('tr')
    // var td = document.createElement('td')
    // td.innerText = payment.id
    // tr.appendChild(td)
    // tbody.innerHTML += `
    var tr = document.createElement('tr')
    tr.innerHTML = `
      <td>${payment.i}</td>
      <td>${formatCurrency(payment.capital_payment)}</td>
      <td>${formatCurrency(payment.interest_payment)}</td>
      <td>${formatCurrency(payment.amount_left)}</td>
      <th>${formatCurrency(payment.payment)}</th>
    `;
    // tr.parentElement == undefined
    tbody.appendChild(tr)
  }
  // var tfoot = document.querySelector('.js-payments-table tfoot')
  var tfoot = document.getElementById('tfoot')
  var totals = renderTotals(payments)
  tfoot.innerHTML = `<tr>
    <td>Suma</td>
    <th>${formatCurrency(totals.capital)}</th>
    <th>${formatCurrency(totals.interest)}</th>
    <th></th>
    <th>${formatCurrency(totals.payments)}</th>
  </tr>`

}