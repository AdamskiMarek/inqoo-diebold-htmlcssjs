
var cartItems = [
  // {
  //   product_id: '123',
  //   product: products[0],
  //   amount: 2,
  //   subtotal: 20
  // }
]

var cartTotal = 0;

// Automated test
addToCart('123')
addToCart('123')
addToCart('234')
// Expect:
// - 2 x Product 123
// - 1 x Product 234
// - totals calculated
console.log(cartItems);
console.log(cartTotal);

function addToCart(product_id) {
  // Find product
  var product = getProductById(product_id)
  var item;

  // If product is in cart - update amount
  for (let line of cartItems) {
    if (line.product_id === product_id) {
      item = line; break;
    }
  }
  // If product not in cart - add product
  if (item === undefined) {
    cartItems.push({
      product_id,
      product,
      amount: 1,
      subtotal: product.price * 1
    })
  } else {
    item.amount++
    item.subtotal = item.product.price * item.amount
  }
  // Calculate subtotal, total
  cartTotal = 0;
  for (let line of cartItems) {
    cartTotal += line.subtotal
  }

  renderCart()
}

function renderCart() {
  var itemElem = document.querySelector('.js-cart-items')
  itemElem.innerHTML = ''
  for (let line of cartItems) {
    itemElem.innerHTML += renderCartItem(line)
  }
}

function formatPrice(nett){
  return (nett / 100).toFixed(2)
}

function renderCartItem(item) {
  var item = /* html */ `<li data-product-id="${item.product_id}" class="list-group-item js-product d-flex justify-content-between lh-sm">
  <div>
    <h6 class="my-0">${item.product.name}</h6>
    <small class="text-muted">Brief description</small>
  </div>
  <span class="text-muted">${item.amount} x ${formatPrice(item.product.price)} = ${
    formatPrice(item.subtotal)}</span>
  <span class="close">&times;</span>
</li>`
  return item
}

function getProductById(product_id) {
  for (let item of products) {
    if (item.id === product_id) {
      return item
    }
  }
}


// // Remove items - naive
// var items = document.querySelectorAll('.js-cart-items              .js-product')

// for (let item of items) {

//   var closeBtn = item.querySelector('.close')
//   closeBtn.addEventListener('click', function (event) {
//     console.log(item, event.target)
//     item.remove()
//   })
// }

// elem.getAttribute('data-product-id')
// elem.dataset.productId

