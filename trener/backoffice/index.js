
class View {

  /** @type HTMLElement */
  el

  /**
   * Create view
   * @param { string | DOMElement } [el] 
   */
  constructor(el = document.createElement('div')) {
    this.el = typeof el === 'string' ? document.querySelector(el) : el;
    if (!this.el) { throw new Error('View mount element not found ' + el) }
  }

  listenTo(model) {
    this.model = model
    this.model.addEventListener('model_changed', () => {
      this.render()
    })
  }

}

class ProductsView extends View {
  /** @type ProductsModel */ model = null

  constructor(el) { super(el) }

  render() {
    this.el.innerHTML = ''
    this.model.getItems().forEach(product => {
      const productView = new ProductView()
      productView.model = product
      this.el.append(productView.el)
      productView.render()
    })
  }
}
class ProductView extends View {
  /** @type Product */  model

  constructor(el) { super(el) }

  render() {
    this.el.outerHTML = /* html */`<div class="list-group-item" data-product-id="${this.model.id}">
    <span>${this.model.name} </span>
    </div > `
  }
}

class ApplicationController {

  productsModel = new ProductsModel(produts)
  productsView = new ProductsView('#productListEl')
  productsSearchInput = document.querySelector('#productsSearchInput')

  constructor() {
    this.productsView.listenTo(this.productsModel)
    this.productsView.render()

    this.productsSearchInput.addEventListener('input', (event) => {
      const query = event.currentTarget.value
      this.productsModel.filter(query)
    })
  }
}

new ApplicationController()