
/**
 * @typedef Product
 * @property {number} id
 * @property {string} name
 * @property {number} price_nett
 * @property {string} [description]
 */

/** @type Product[] */
var produts = [
  { id: '123', name: 'Product Test 123', price_nett: 1230, description: '' },
  { id: '234', name: 'Product Testing 234', price_nett: 2340, description: '' },
  { id: '345', name: 'Product Example Test 345', price_nett: 3450, description: '' },
]

class ModelChanged extends Event {
  type = 'model_changed'
}

class ProductsModel extends EventTarget {
  /** 
   * @type Product[]
   * @protected
   */
  _data = [];

  /** 
   * @type Product[]
   * @protected
   */
  _results = this._data

  constructor(products) {
    super()
    this._data = products
    this._results = this._data
  }

  /**
   * 
   * @returns Product[]
   */
  getItems() {
    return this._results
  }

  /**
   * Filer products
   * @param {string} query 
   */
  filter(query) {
    this._results = this._data.filter(p => p.name.toLocaleLowerCase().includes(query.toLocaleLowerCase()))
    this.dispatchEvent(new ModelChanged('model_changed', this._results))
  }
}